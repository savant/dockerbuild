#!/bin/sh
find ./artifacts -mindepth 1 ! -iname ".*" -exec rm -rf {} \;
docker build . --tag a13brd_build
if [ "$?" = "0" ]; then
	docker run --privileged --rm -v $(pwd):/mnt a13brd_build
fi
